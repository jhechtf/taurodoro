import { writable } from "svelte/store";
import type { Timer } from "./timers";

export type ActiveTimerStatuses = 'running' | 'stopped' | 'paused';

export class ActiveTimer implements Timer {
  #elapsed: number = 0;
  #status: ActiveTimerStatuses = 'stopped';
  constructor(
    public id: string = crypto.randomUUID(),
    public work: number = 25 * 60 * 1_000,
    public rest: number = 5 * 60 * 1_000,
    public name: string = "Default",
  ) {
  }

  get status() {
    return this.#status;
  }
  get elapsed() {
    return this.#elapsed;
  }
  get remaining() {
    return this.rest + this.work - this.elapsed;
  }

  start() {
    this.#status = 'running';
  }

  stop() {
    this.#status = 'stopped';
    this.#elapsed = 0;
  }

  pause() {
    this.#status = 'paused';
  }
}

export default writable([]);
