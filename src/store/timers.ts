import { writable } from "svelte/store";
export interface Timer {
  id: string;
  work: number;
  rest: number;
  name: string;
}
export default writable<Timer[]>([{
  id: crypto.randomUUID(),
  name: "Default",
  work: 1_000 * 60 * 25,
  rest: 1_000 * 60 * 5,
}, {
  id: crypto.randomUUID(),
  name: "Hour",
  work: 1_000 * 60 * 50,
  rest: 1_000 * 60 * 10,
}]);
