///<reference lib="es2020.intl" />
export const relativeNumberFormatter = new Intl.RelativeTimeFormat(undefined, {
  numeric: "always",
});

const SECOND = 1_000;
const MINUTE = SECOND * 60;
const HOUR = MINUTE * 60;

/**
 * @param ms a duration of time represented in _miliseconds_
 */
export function milisecondsToDurationParts(
  ms: number,
): Intl.RelativeTimeFormatPart[] {
  const parts: Intl.RelativeTimeFormatPart[] = [];

  if (ms >= HOUR) {
    let hours = Math.floor(ms / HOUR);
    parts.push({
      type: 'integer',
      value: hours.toString(),
      unit: 'hour'
    });
    ms -= hours * HOUR;
  }

  if (ms >= MINUTE) {
    let minutes = Math.floor(ms / MINUTE);
    parts.push({
      type: 'integer',
      value: minutes.toString(),
      unit: 'minute'
    });
    ms -= minutes * MINUTE;
  }

  if (ms >= 0) {
    let seconds = ms / SECOND;
    parts.push({
      type: 'integer',
      value: seconds.toString(),
      unit: 'second',
    });
  }

  return parts;
}
